%bcond_with bootstrap

Summary:        Server-side API for handling HTTP requests and responses
Name:           jakarta-servlet
Version:        6.1.0
Release:        1%{?dist}
License:        (EPL-2.0 or GPLv2 with exceptions) and ASL 2.0
URL:            https://github.com/eclipse-ee4j/servlet-api
Source0:        https://github.com/eclipse-ee4j/servlet-api/archive/%{version}-RELEASE/servlet-api-%{version}.tar.gz

%if %{with bootstrap}
BuildRequires:  javapackages-bootstrap
%else
BuildRequires:  maven-local
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin)
%endif

BuildArch:      noarch
ExclusiveArch:  %{java_arches} noarch

Provides:       glassfish-servlet-api = %{version}-%{release}

%description
Jakarta Servlet defines a server-side API for handling HTTP requests
and responses.

%{?javadoc_package}



%prep
%setup -q -n servlet-api-%{version}-RELEASE
%pom_remove_parent . api
%pom_disable_module spec

cp -pr api/src/main/java/jakarta api/src/main/java/javax
sed -i -e 's/jakarta\./javax./g' $(find api/src/main/java/javax -name *.java)
%pom_xpath_replace pom:instructions/pom:Export-Package \
  '<Export-Package>jakarta.servlet.*,javax.servlet.*;version="4.0.0"</Export-Package>' api

%mvn_package jakarta.servlet:servlet-parent __noinstall

%pom_remove_plugin -r :formatter-maven-plugin
%pom_remove_plugin -r :impsort-maven-plugin
%pom_remove_plugin -r :maven-enforcer-plugin
%pom_remove_plugin -r :maven-javadoc-plugin
%pom_remove_plugin -r :maven-source-plugin

%mvn_alias jakarta.servlet:jakarta.servlet-api \
    javax.servlet:javax.servlet-api \
    javax.servlet:servlet-api

%mvn_file :{*} %{name}/@1 glassfish-servlet-api



%build
%mvn_build



%install
%mvn_install



%files -f .mfiles
%license LICENSE.md NOTICE.md
%doc README.md



%changelog
* Fri Sep 20 2024 Upgrade Robot <upbot@opencloudos.tech> - 6.1.0-1
- Upgrade to version 6.1.0

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.0.0-4
- Rebuilt for loongarch release

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.0.0-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.0.0-2
- Rebuilt for OpenCloudOS Stream 23.05

* Sun Apr 23 2023 Zhao Zhen <jeremiazhao@tencent.com> - 5.0.0-1
- initial
